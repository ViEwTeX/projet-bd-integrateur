drop table TableFestival;
drop table TableHebergement;

create table TableFestival (
    nomF varchar(100),
    typeF varchar(60),
    lien varchar(250),
    ville varchar(60),
    dateD varchar(20),
    dateF varchar(20),
    postal varchar(5),
    longitude varchar(20), 
    latitude varchar(20),
    constraint pk_TF primary key (nomF)
);

create table TableHebergement(
    nomH varchar(100),
    typeH varchar(60),
    lien varchar(250),
    classement varchar(250),
    adresse varchar(100),
    ville varchar(100),
    tel varchar(15),
    nbc varchar(20),
    nbe varchar(20),
    nbr varchar(20),
    nbvv varchar(20),
    postal varchar(8),
    longitude varchar(20), 
    latitude varchar(20),
    constraint pk_TH primary key (nomH)
);


create or replace trigger InsertionFestival
  before insert on TableFestival
  for each row
  begin
  
  insert into Festival values (seq_festival.nextval,:new.nomF,0,TO_DATE(:new.dateD,'dd/mm/yyyy'),TO_DATE(:new.dateF,'dd/mm/yyyy'),:new.typeF,:new.lien,:new.postal,:new.ville,'France',10,15,20,20,30,40,'Disponible',TO_NUMBER(:new.longitude),TO_NUMBER(:new.latitude),1);
  
  end;
  /
  
create or replace trigger InsertionHebergement
  before insert on TableHebergement
  for each row
  Declare 
    typeH varchar(20);
    rand number;
    nbc number;
    nbe number;
    nbr number;
    nbvv number;
    numHebergeur number;
    nbHebergeur number;
    nbdisp number;
    maxi number;
  begin

  typeH := :new.typeH;
  select count(*) into nbHebergeur from Hebergeur;
  select floor(dbms_random.value(1, nbHebergeur)) into numHebergeur from Festival where numFestival  = 1;
  select max(numDispo) into maxi from Disponibilite;
  nbdisp := maxi;


  insert into Hebergement values (seq_hebergement.nextval,:new.nomH,0,TO_NUMBER(:new.classement),:new.tel,:new.lien,:new.adresse,:new.postal,:new.ville,'France','Description',TO_NUMBER(:new.longitude),TO_NUMBER(:new.latitude),numHebergeur);
  
  if typeH like 'Hotel' then 
    nbc := TO_NUMBER(:new.nbc);
    insert into Hotel values (seq_hotel.nextval,10,30,seq_hebergement.currval);
    while nbc > 0 
    loop
      insert into Logement values (seq_logement.nextval,'Chambre Simple, vue sur la mer magnifique','Simple',1,1,1,100,seq_hebergement.currval);
        while nbdisp > 0 loop
            insert into LogementDispo values (seq_logement.currval,nbdisp);
            nbdisp := nbdisp - 1;
        end loop;
      nbdisp := maxi;
      nbc := nbc - 1;
    end loop;

  elsif typeH like 'Camping' then
    nbe := TO_NUMBER(:new.nbe);
    insert into Camping values (seq_camping.nextval,25,30,seq_hebergement.currval);
    while nbe > 0
    loop
      insert into Logement values (seq_logement.nextval,'Camping Paradis','Autre',4,4,4,202,seq_hebergement.currval);
        while nbdisp > 0 loop
            insert into LogementDispo values (seq_logement.currval,nbdisp);
            nbdisp := nbdisp - 1;
        end loop;
       nbdisp := maxi;   
    nbe := nbe - 1;
    end loop;

  elsif typeH like 'Residence' then
    nbr := TO_NUMBER(:new.nbr);
    insert into Residence values (seq_residence.nextval,20,100,seq_hebergement.currval);
  while nbr > 0
  loop
    insert into Logement values (seq_logement.nextval,'Residence Im2ag','Autre',4,4,4,202,seq_hebergement.currval);
      while nbdisp > 0 loop
          insert into LogementDispo values (seq_logement.currval,nbdisp);
          nbdisp := nbdisp - 1;
      end loop;
      nbdisp := maxi; 
    nbr := nbr - 1;
  end loop;

  elsif typeH like 'Village' then
    nbvv := TO_NUMBER(:new.nbvv);
    insert into VillageVacances values (seq_village.nextval,8,16,'Location',36,seq_hebergement.currval);
  while nbvv > 0
  loop
    insert into Logement values (seq_logement.nextval,'Chambre village de vacances','Simple',4,4,4,202,seq_hebergement.currval);
      while nbdisp > 0 loop
          insert into LogementDispo values (seq_logement.currval,nbdisp);
          nbdisp := nbdisp - 1;
      end loop;
      nbdisp := maxi; 
    nbvv := nbvv - 1;
  end loop;

  elsif typeH like 'Parc' then
    nbvv := 5;
    insert into ParcResidentiel values (seq_parcresidentiel.nextval,38,60,seq_hebergement.currval);
  while nbvv > 0
  loop
    insert into Logement values (seq_logement.nextval,'Parc Residentiel du Cannet','Autre',4,4,4,202,seq_hebergement.currval);
      while nbdisp > 0 loop
          insert into LogementDispo values (seq_logement.currval,nbdisp);
          nbdisp := nbdisp - 1;
      end loop;
      nbdisp := maxi; 
    nbvv := nbvv - 1;
  end loop;

  end if;

  end;
  /
  