drop trigger PANIER_MINIMUM;
drop trigger TYPE_HOTEL;
drop trigger TYPE_VILLAGE_VACANCES;
drop trigger TYPE_CAMPING;
drop trigger AVIS_HEBERGEMENT;
drop trigger TYPE_RESIDENCE;
drop trigger TYPE_PARC_RESIDENTIEL;
drop trigger AVIS_FESTIVAL;

-- Trigger
-- 1. Au moins reserver un logement + un festival
create or replace trigger PANIER_MINIMUM
	before update of etatReservation on Reservation
	for each row
	declare
		nbHebergement integer;
		nbFestival integer;
	begin
		select count(*) into nbFestival from FestivalReservation where numReservation = :new.numReservation;
		select count(*) into nbHebergement from ReservationLogement where numReservation = :new.numReservation;
		if nbFestival < 1 or nbHebergement < 1 then
			RAISE_APPLICATION_ERROR( -20001, 'Il faut reserver au moin un logement et un festival!' ); 
		end if;
	end;
/
-- jeu de test:

-- select count(distinct numLogement), count(distinct numFestival) from reservation natural join festivalreservation natural join reservationLogement;
-- insert into Reservation values (100,TO_DATE('2020/01/21 14:25:32' , 'yyyy/mm/dd hh24:mi:ss'),null,'En cours',100,2,2,1);
-- insert into FestivalReservation values (3,100,0,0,2,TO_DATE('2020/01/22', 'yyyy/mm/dd'));
-- update reservation set etatReservation = 'Valide' where numReservation = 100;

-- 2. type de chambre doit etre coherent avec type de hebergement [Hotel]
create or replace trigger TYPE_HOTEL
	before insert or update on logement
	for each row
	declare
		nbHotel integer;

	begin
	select count(*) into nbHotel from hotel where numHebergement = :new.numHebergement;
	if nbHotel > 0 then
		if :new.type_chambre not in ('Simple','Double','Familiale') then
			RAISE_APPLICATION_ERROR( -20002, 'Veuillez saisir un type de logement valide pour un hotel!' );
		end if;
	end if;
	end;
/
-- jeu de test:
-- insert into Hotel values (2,15,45,6);
-- insert into Logement values (seq_logement.nextval,'Chambre Simple, vue montagne','Dortoir',1,1,1,2);
-- insert into Logement values (seq_logement.nextval,'Chambre Simple, vue montagne','Double',1,1,1,2);

-- 3. type de chambre doit etre coherent avec type de hebergement [residence]
create or replace trigger TYPE_VILLAGE_VACANCES
	before insert or update on logement
	for each row
	declare
		nbResidence integer;

	begin
	select count(*) into nbResidence from VILLAGEVACANCES where numHebergement = :new.numHebergement;
	if nbResidence > 0 then
		if :new.type_chambre not in ('Simple','Double','Familiale', 'Dortoir') then
			RAISE_APPLICATION_ERROR( -20003, 'Veuillez saisir un type de logement valide pour un village de vacances!' );
		end if;
	end if;
	end;
/
-- jeu de test:
-- insert into Logement values (seq_logement.nextval,'Chambre Simple, vue montagne','Autre',1,1,1,3);

-- 4. type de chambre doit etre coherent avec type de hebergement [camping]
create or replace trigger TYPE_CAMPING
	before insert or update on logement
	for each row
	declare
		nbCamping integer;

	begin
	select count(*) into nbCamping from camping where numHebergement = :new.numHebergement;
	if nbCamping > 0 then
		if :new.type_chambre <> 'Autre' then
			RAISE_APPLICATION_ERROR( -20004, 'Veuillez saisir un type de logement valide pour un camping!' );
		end if;
	end if;
	end;
/

-- jeu de test:
-- insert into Logement values (seq_logement.nextval,'Chambre Simple, vue montagne','Simple',1,1,1,1);

-- 5. type de chambre doit etre coherent avec type de hebergement [residence]
create or replace trigger TYPE_RESIDENCE
	before insert or update on logement
	for each row
	declare
		nbResidence integer;
	begin
	select count(*) into nbResidence from residence where numHebergement = :new.numHebergement;
	if nbResidence > 0 then
		if :new.type_chambre <> 'Autre' then
			RAISE_APPLICATION_ERROR( -20005, 'Veuillez saisir un type de logement valide pour un residence!' );
		end if;
	end if;
	end;
/

-- jeu de test:
-- insert into Logement values (seq_logement.nextval,'Chambre Simple, vue montagne','Simple',1,1,1,4);

-- 6. type de chambre doit etre coherent avec type de hebergement [ParcResidentiel]
create or replace trigger TYPE_PARC_RESIDENTIEL
	before insert or update on logement
	for each row
	declare
		nbParcResidentiel integer;
	begin
	select count(*) into nbParcResidentiel from ParcResidentiel where numHebergement = :new.numHebergement;
	if nbParcResidentiel > 0 then
		if :new.type_chambre <> 'Autre' then
			RAISE_APPLICATION_ERROR( -20006, 'Veuillez saisir un type de logement valide pour un parc residentiel!' );
		end if;
	end if;
	end;
/
-- jeu de test:
-- insert into Logement values (seq_logement.nextval,'Chambre Simple, vue montagne','Simple',1,1,1,5);
-- insert into Logement values (seq_logement.nextval,'Chambre Simple, vue montagne','Double',1,1,1,5);

-- 7. avis hebergement doit etre cree apres le sejour est passe.

create or replace trigger AVIS_HEBERGEMENT
	before insert on AvisHebergement
	for each row
	declare
		dateFin date;
	begin
		select dateFin into dateFin from ReservationLogement where numReservation = :new.numReservation;
		if sysdate <= dateFin then
		RAISE_APPLICATION_ERROR( -20007, 'Pour laisser un avis a un hebergement, il faut que la sejour soit passe!');
		end if;
	end;
/

-- jeu de test:
-- insert into Reservation values (seq_reservation.nextval,TO_DATE('2020/01/01 14:25:32' , 'yyyy/mm/dd hh24:mi:ss'),null,'En cours',100,2,2,1);
-- insert into ReservationLogement values (6,21,TO_DATE('2020/01/02', 'yyyy/mm/dd'),TO_DATE('2020/01/04', 'yyyy/mm/dd'));
-- insert into AvisHebergement values (seq_avishebergement.nextval,'Donde esta la playa',4,2);


-- 8. avis festival doit etre cree le plus tot dans le jour apres le festival est fini. 
create or replace trigger AVIS_FESTIVAL
	before insert on AVISFESTIVAL
	for each row
	declare
		dateFinRes date;
		dateFinFestival date;
	begin
		select max(DATEPLACERES) into dateFinRes from FestivalReservation where numReservation = :new.numReservation and numFestival = :new.numFestival;
		select dateFin into dateFinFestival from festival where numFestival = :new.numFestival;
		if sysdate <= dateFinRes or sysdate <= dateFinRes or dateFinRes is null then
			RAISE_APPLICATION_ERROR( -20008, 'Pour laisser un avis a un festival, il faut que la festival soit passe!');
		end if;
	end;
/

-- jeu de test:
-- insert into AvisFestival values (seq_avisfestival.nextval,'Donde esta la playa',3,2);
