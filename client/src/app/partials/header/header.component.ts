import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AngularFireAuth} from "@angular/fire/auth";
import {SidebarService} from "../../services/sidebar.service";
import {auth, User} from 'firebase';
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  private user: User;
  @Output() public panier = new EventEmitter();
  constructor(private afAuth: AngularFireAuth,private sidenav: SidebarService, private authService: AuthService) {
    this.afAuth.user.subscribe((user) => {
      if (user) {
        this.user = user;
        if(localStorage.getItem('userId') !== user.uid) {
          this.authService.authentificationServer(user.uid, user.displayName, user.displayName, user.phoneNumber, user.email).subscribe(r => {
            localStorage.setItem('userId', r);
            // console.log('persId', localStorage.getItem('persId'), 'userId',localStorage.getItem('userId'));
          });
        }
        localStorage.setItem('persId', user.uid);
      } else {
        localStorage.removeItem('userId');
        localStorage.removeItem('persId');
      }
    });
  }

  ngOnInit() {
  }
  sendMessageToParent() {
    this.panier.emit()
  }
  async loginGoogle() {
    await this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());

//    location.reload();
  }

  async deLoginGoogle() {
    await this.afAuth.auth.signOut();
    await this.afAuth.auth.app.delete();
    location.reload();
  }
  isUserConnected(): boolean {
    return !!this.user;
    // !! pour que quand variable est null et il est faux
  }
}
