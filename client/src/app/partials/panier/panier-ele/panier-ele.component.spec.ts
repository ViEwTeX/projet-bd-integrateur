import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanierEleComponent } from './panier-ele.component';

describe('PanierEleComponent', () => {
  let component: PanierEleComponent;
  let fixture: ComponentFixture<PanierEleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanierEleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanierEleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
