import { Component, OnInit } from '@angular/core';
import {ReservationService} from "../../services/reservation.service";
import {ActivatedRoute, Router} from "@angular/router";
import {DialogMsgComponent} from "../dialog-msg/dialog-msg.component";
import {MatDialog} from "@angular/material";
import {DataService} from "../../services/data.service";


@Component({
  selector: 'app-validation-panier',
  templateUrl: './validation-panier.component.html',
  styleUrls: ['./validation-panier.component.scss']
})
export class ValidationPanierComponent implements OnInit {
  id;
  msg;

  constructor(private router: Router, private route: ActivatedRoute, private serviceReservation: ReservationService, public dialog: MatDialog, private dataService: DataService) {
    this.id = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() {

  }

  payer() {
    this.serviceReservation.validerReservation(this.id).subscribe(
      () => {
        this.dataService.refreshPanier();
      }
    );
    this.msg = 'Vous avez ajouté avec succès!';
    this.openMsgDialog();
  }
  openMsgDialog(): void {
    const dialogRef = this.dialog.open(DialogMsgComponent, {
      width: '400px',
      data: {
        msg: this.msg
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.router.navigateByUrl("").then(() => {
        location.reload();
      });

    });
  }

  retour() {
    this.router.navigateByUrl("");
  }
}
