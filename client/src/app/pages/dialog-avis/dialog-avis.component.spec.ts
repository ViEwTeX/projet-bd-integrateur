import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogAvisComponent } from './dialog-avis.component';

describe('DialogAvisComponent', () => {
  let component: DialogAvisComponent;
  let fixture: ComponentFixture<DialogAvisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogAvisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogAvisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
