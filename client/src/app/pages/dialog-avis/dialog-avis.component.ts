import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {DialogMsgComponent} from "../dialog-msg/dialog-msg.component";
export interface DialogAvisData {
  name: string;
  note: number;
}
@Component({
  selector: 'app-dialog-avis',
  templateUrl: './dialog-avis.component.html',
  styleUrls: ['./dialog-avis.component.scss']
})
export class DialogAvisComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) private data: DialogAvisData,
              private dialogRef: MatDialogRef<DialogMsgComponent>) { }

  ngOnInit() {
  }

}
