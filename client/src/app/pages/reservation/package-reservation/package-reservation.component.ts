import {Component, Input, OnInit} from '@angular/core';
import {DialogAvisComponent} from "../../dialog-avis/dialog-avis.component";
import {MatDialog} from "@angular/material";

@Component({
  selector: 'app-package-reservation',
  templateUrl: './package-reservation.component.html',
  styleUrls: ['./package-reservation.component.scss']
})
export class PackageReservationComponent implements OnInit {
  @Input() data;
  @Input() commentable: boolean;
  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }
  openSeeMore(reservation) {
    if (reservation )
      console.log(reservation.openSeeMore);
    reservation.openSeeMore = !reservation.openSeeMore;
  }
  openAvisDialog(objet): void {
    const dialogRef = this.dialog.open(DialogAvisComponent, {
      width: '400px',
      data: {
        name: objet.nom,
        note: objet.noteMoyenne
      }
    });
    /*
        dialogRef.afterClosed().subscribe(result => {
        });*/
  }
}
