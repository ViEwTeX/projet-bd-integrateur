import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PackageReservationComponent } from './package-reservation.component';

describe('PackageReservationComponent', () => {
  let component: PackageReservationComponent;
  let fixture: ComponentFixture<PackageReservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PackageReservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PackageReservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
