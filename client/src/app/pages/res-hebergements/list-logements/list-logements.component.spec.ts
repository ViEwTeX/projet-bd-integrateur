import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListLogementsComponent } from './list-logements.component';

describe('ListLogementsComponent', () => {
  let component: ListLogementsComponent;
  let fixture: ComponentFixture<ListLogementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListLogementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListLogementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
