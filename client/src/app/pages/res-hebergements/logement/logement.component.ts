import {Component, Input, OnInit, SimpleChange, SimpleChanges} from '@angular/core';
import {HebergementService} from "../../../services/hebergement.service";
import {ReservationComponent} from "../../reservation/reservation.component";
import {ReservationService} from "../../../services/reservation.service";
import {DateFormatPipe} from "../../../utils/date-format.pipe";
import {DataService} from "../../../services/data.service"
import {MatDialog} from "@angular/material";
import {Router} from "@angular/router";
import {DialogMsgComponent} from "../../dialog-msg/dialog-msg.component";
import {map} from "rxjs/internal/operators";

@Component({
  selector: 'app-logement',
  templateUrl: './logement.component.html',
  styleUrls: ['./logement.component.scss']
})
export class LogementComponent implements OnInit {
  @Input() data;
  logement;
  nbAdulte;
  nbEnfant;
  @Input() dateRes;
  dateD;
  dateF;
  constructor(private serviceHebergement: HebergementService, private serviceReservation: ReservationService,
              private datePipe: DateFormatPipe, public dataService: DataService, private dialog: MatDialog, private routerL:Router
  ) { }

  ngOnInit() {
    this.dateD = this.datePipe.transform(this.dateRes.dateDebut);
    this.dateF = this.datePipe.transform(this.dateRes.dateFin);
  }
  onChangeNbAdulte(value) {
    this.nbAdulte = value;
  }
  onChangeNbEnfant(value) {
    this.nbEnfant = value;
  }
  ajouterPanier() {
    console.log(this.dateRes);
    this.serviceReservation.addLogement(localStorage.getItem('userId'), this.data.numLogement,this.datePipe.transform(this.dateRes.dateDebut), this.datePipe.transform(this.dateRes.dateFin), this.nbEnfant? this.nbEnfant: 0, this.nbAdulte? this.nbAdulte: 0).
    subscribe(
      data => {
        this.dataService.refreshPanier();
        this.openMsgDialog(false,'Vous avez ajouté un logement avec succès! ');
      },
      error => {

        if (error){
          if (error && error.error && error.message) {
            this.openMsgDialog(true, error.error.message);
          } else {
            this.openMsgDialog(true, 'Contraintes non-respectées!');
          }
        }
      }
    );
  }

  openMsgDialog(error, msg): void {
    const dialogRef = this.dialog.open(DialogMsgComponent, {
      width: '400px',
      data: {
        error: error,
        msg: msg
      }
    });
/*    if (!error) {
      dialogRef.afterClosed().subscribe(result => {
        this.router.navigateByUrl('hebergements?ville='+this.festival.ville);
      });
    }*/

  }

  transformDate(date){
    const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    return new Date(date).toLocaleDateString('fr-FR', options);
  }

  ngOnChanges(changes: SimpleChanges) {
    this.dateD = changes['dateRes']['dateDebut'];
    this.dateF = changes['dateRes']['dateFin'];

  }
}
