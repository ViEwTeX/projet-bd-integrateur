import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/index";

@Injectable({
  providedIn: 'root'
})
export class GenerateAvatarService {
  URL = 'https://eu.ui-avatars.com/api/';
  constructor(private http: HttpClient) {
  }
  getAvatar(name): Observable<any> {
    return this.http.get(this.URL + name, { responseType: 'blob' });
  }
}
