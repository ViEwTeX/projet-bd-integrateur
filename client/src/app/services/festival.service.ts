import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs/index";

const URL = environment.api + '/festivals';

@Injectable({
  providedIn: 'root'
})

export class FestivalService {
  constructor(private http: HttpClient) {
  }
  getFestivals(taillePage, numPage, motCles?, ville?, categorie?, dateDebut?, dateFin? ) : Observable <any> {
    let params = new HttpParams();
    params = params.set('taillePage', taillePage);
    params = params.set('numPage', numPage);
    if (motCles) {
      params = params.set('motsCles', motCles);
    }
    if (ville) {
      params = params.set('ville', ville);
    }
    if (categorie) {
      params = params.set('categorie', categorie);
    }
    if (dateDebut) {
      params = params.set('dateDebut', dateDebut);
    }
    if (dateFin) {
      params = params.set('dateFin', dateFin);
    }
    return this.http.get(URL, {params: params});
  }
  getDomaines(): Observable<any> {
    return this.http.get(URL + '/domaines');
  }
  getFestivalById(id): Observable<any> {
    let params = new HttpParams();
    params = params.set('id', id);
    return this.http.get(environment.api + '/festival', {params: params});
  }
}
