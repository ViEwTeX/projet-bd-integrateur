import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs/index";
const URL = environment.api + '/hebergementm';
@Injectable({
  providedIn: 'root'
})
export class HebergementService {

  constructor(private http: HttpClient) { }
  getHebergements(ville): Observable<any> {
    let params = new HttpParams();
    params = params.set('ville', ville);
    return this.http.get(URL, {params: params});
  }
  getHebergementById(id): Observable<any> {
    let params = new HttpParams();
    params = params.set('numHebergement', id);
    return this.http.get(environment.api + '/hebergement', {params: params});
  }
  getHebergementByNumLogement(numLogement): Observable<any> {
    let params = new HttpParams();
    params = params.set('numLogement', numLogement);
    return this.http.get(environment.api + '/hebergementm', {params: params});
  }
  getLogements(id): Observable<any> {
    let params = new HttpParams();
    params = params.set('numHebergement', id);
    return this.http.get(environment.api + '/logement', {params: params});
  }
}
