import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs/index";
import {environment} from "../../environments/environment";
const URL = environment.api;
@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  constructor(private http: HttpClient) { }
  getPanier(numPersonne): Observable<any> {
    let params = new HttpParams();
    params = params.set('numPersonne', numPersonne);
    return this.http.get(URL + '/panier', {params: params});
  }
  validerPanier(numReservation): Observable<any> {
    let params = new HttpParams();
    // TODO
    params.set('numReservation', numReservation);
    return this.http.post(URL + '/panier', {numReservation: numReservation});
  }
  annulerPanier() {
    let body = {};
    //TODO
    return this.http.post(URL,body);
  }
  getResPassees(): Observable<any> {
    let params = new HttpParams();
    return this.http.get(URL, {params: params});
  }
  getResFutur(): Observable<any> {
    let params = new HttpParams();
    return this.http.get(URL, {params: params});
  }
  getAllRes(): Observable<any> {
    let params = new HttpParams();
    return this.http.get(URL, {params: params});
  }
  getResAnnulees(): Observable<any> {
    let params = new HttpParams();
    return this.http.get(URL, {params: params});
  }
  deleteFestDePanier(numF, numR, dateRF): Observable<any>{
    let params = new HttpParams();
    params = params.set('numF', numF);
    params = params.set('numR', numR);
    params = params.set('dateRF', dateRF);
    return this.http.post(URL + '/reservation/removeFestival', '',{params: params});
  }
  deleteLogeDePanier(numR, numL, dateD, dateF): Observable<any>{
    let params = new HttpParams();
    params = params.set('numR',numR);
    params = params.set('numL',numL);
    params = params.set('dateD',dateD);
    params = params.set('dateF',dateF);
    return this.http.post(URL + '/reservation/removeLogement', '',{params: params});
  }
  addFestival(numU, numF, nbC0, nbC1, nbC2, dateD, dateF): Observable<any> {
    let params = new HttpParams();
    params = params.set('numU', numU);
    params = params.set('numF', numF);
    params = params.set('nbC0', nbC0);
    params = params.set('nbC1', nbC1);
    params = params.set('nbC2', nbC2);
    params = params.set('dateD', dateD);
    params = params.set('dateF', dateF);
    return this.http.post(URL + '/reservation/addFestival', '',{params: params});
  }

  addLogement(numU, numL, dateD, dateF, nbEnfant, nbAdulte): Observable<any> {
    let params = new HttpParams();
    params = params.set('numU', numU);
    params = params.set('numL', numL);
    params = params.set('dateD', dateD);
    params = params.set('dateF', dateF);
    params = params.set('nbEnfants', nbEnfant);
    params = params.set('nbAdultes', nbAdulte);
    return this.http.post(URL + '/reservation/addLogement', '',{params: params});
  }

  validerReservation(id): Observable<any>{
    let params = new HttpParams();
    params = params.set('numReservation', id);
    return this.http.post(URL + '/reservation/validerServlet', '',{params: params});
  }

  checkExpiration(numUtilisateur): Observable<any> {
    let params = new HttpParams();
    params = params.set('numUtilisateur',numUtilisateur);
    return this.http.get(URL + '/reservation/validation', {params : params});
  }
}
