// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  tmdbKey: 'ca571d61521790487fdb62e791d40f66',
  firebase: {
      apiKey: "AIzaSyAoXk8gZvcwFDM8Bhq28nVjWHqGUchWj4w",
      authDomain: "festibed-b83c8.firebaseapp.com",
      databaseURL: "https://festibed-b83c8.firebaseio.com",
      projectId: "festibed-b83c8",
      storageBucket: "festibed-b83c8.appspot.com",
      messagingSenderId: "751923916176",
      appId: "1:751923916176:web:142332d6ff372c971fd715",
      measurementId: "G-1Z61V56MT3"

  },
  api: 'http://localhost:8080',
  googleMapsApiKey: 'AIzaSyA7JtOK5E1INPYWk6AhyBAC9s_HlkhpAOc'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
