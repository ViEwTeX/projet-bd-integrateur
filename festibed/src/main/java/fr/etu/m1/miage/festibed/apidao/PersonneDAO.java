package fr.etu.m1.miage.festibed.apidao;

import java.sql.SQLException;
import java.util.Iterator;

import fr.etu.m1.miage.festibed.model.Personne;

public interface PersonneDAO {
	
	Iterator<Personne> getPersonnes(String numPersonne, String nom, String prenom, String telephone, String mail);
	
	// Iterator<String> getDomaines();

	Personne getPersonneById(int numPersonne);

	int ajouterPersonne(Personne user) throws SQLException;

}
