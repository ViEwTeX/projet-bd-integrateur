package fr.etu.m1.miage.festibed.service;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import com.google.gson.Gson;

import fr.etu.m1.miage.festibed.exception.CreationResException;
import fr.etu.m1.miage.festibed.exception.ParameterErrorException;
import fr.etu.m1.miage.festibed.model.Reservation;
import fr.etu.m1.miage.festibed.oracledao.OracleReservationDAO;

public class ReservationService {

	public String rechercherReservationsEnCours(String numPersonne) throws ParameterErrorException {

		ReservationFestivalService reservationFService = new ReservationFestivalService();
		ReservationLogementService reservationService = new ReservationLogementService();
		Reservation res;

		OracleReservationDAO reservationDAO = new OracleReservationDAO();

		Iterator<Reservation> reservations = reservationDAO.getReservations(numPersonne);

		Gson gson = new Gson();
		String jsonResponse = "";
		jsonResponse += "{ \"panier\": [";
		while (reservations.hasNext()) {
			res = reservations.next();

			reservationFService.rechercherFestivalGsonV1(res, res.getNumReservation(), gson);
			reservationService.rechercherLogementGsonV1(res, res.getNumReservation(), gson);
			jsonResponse += gson.toJson(res);

			if (reservations.hasNext()) {
				jsonResponse += ",";
			}
		}

		jsonResponse += "]";
		jsonResponse += "}";

		return jsonResponse;
	}
	
	public void ajouterFestival(String numUtilisateur, String numFestival, String nbPlaceCateg0, String nbPlaceCateg1,
			String nbPlaceCateg2, String dateDebut, String dateFin) throws ParameterErrorException, SQLException, CreationResException {
	//	(int numUtilisateur, int numFestival, int nbPlaceCateg0, int nbPlaceCateg1,
	//		int nbPlaceCateg2, Date dateResa
		int numU, numF, nbC0, nbC1, nbC2;
		try {
			numU = Integer.parseInt(numUtilisateur);
 			numF = Integer.parseInt(numFestival);
 			nbC0 = Integer.parseInt(nbPlaceCateg0);
 			nbC1 = Integer.parseInt(nbPlaceCateg1);
 			nbC2 = Integer.parseInt(nbPlaceCateg2);	
 		} catch (NumberFormatException numberFormatException) {
			throw new ParameterErrorException();
		}

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date dateD, dateF;
		try {
		    dateD = dateFormat.parse(dateDebut);
		    dateF = dateFormat.parse(dateFin);
		} catch (ParseException e) {
			throw new ParameterErrorException();
		}
		long diffInMillies = dateF.getTime() - dateD.getTime();
	    long nbDays = TimeUnit.DAYS.convert(diffInMillies,TimeUnit.MILLISECONDS)+1;
		OracleReservationDAO oracleReservationDAO = new OracleReservationDAO();
		for(int i=0;i<nbDays;i++) {
			Calendar cal = Calendar.getInstance();
	        cal.setTime(dateD);
	        cal.add(Calendar.DATE, i); //minus number would decrement the days
			oracleReservationDAO.ajouterReservationFestival(numU, numF, nbC0, nbC1, nbC2, cal.getTime());
		}
		
		
	}

	public void ajouterLogementReservation(String numLogement, String dateDebut, String dateFin, String numUtilisateur, String nbAdultes, String nbEnfants) throws SQLException, CreationResException, ParameterErrorException {
		int numL, numU, nbAd, nbEn;
		try {
			numL = Integer.parseInt(numLogement);
			numU = Integer.parseInt(numUtilisateur);
			nbAd = Integer.parseInt(nbAdultes);
			nbEn = Integer.parseInt(nbEnfants);
 		} catch (NumberFormatException numberFormatException) {
			throw new ParameterErrorException();
		}

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date dateD, dateF;
		try {
		    dateD = dateFormat.parse(dateDebut);
		    dateF = dateFormat.parse(dateFin);
		} catch (ParseException e) {
			throw new ParameterErrorException();
		}
		OracleReservationDAO oracleReservationDAO = new OracleReservationDAO();
		oracleReservationDAO.ajouterReservationLogement(numL, dateD, dateF, numU, nbAd, nbEn);
				
	}

	public void supprimerFestival(String numFestival, String numReservation, String dateReservation) throws SQLException, ParameterErrorException {
		int numF, numR;
		try {
			numF = Integer.parseInt(numFestival);
			numR = Integer.parseInt(numReservation);
 		} catch (NumberFormatException numberFormatException) {
			throw new ParameterErrorException();
		}

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date dateRF;
		try {
		    dateRF = dateFormat.parse(dateReservation);
		} catch (ParseException e) {
			throw new ParameterErrorException();
		}
		OracleReservationDAO oracleReservationDAO = new OracleReservationDAO();
		oracleReservationDAO.supprimerReservationFestival(numF, dateRF, numR);
	}

	public void supprimerLogement(String numLogement, String numReservation, String dateDebut, String dateFin) throws ParameterErrorException, SQLException {
		int numL, numR;
		try {
			numL = Integer.parseInt(numLogement);
			numR = Integer.parseInt(numReservation);
 		} catch (NumberFormatException numberFormatException) {
			throw new ParameterErrorException();
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date dateD, dateF;
		try {
		    dateD = dateFormat.parse(dateDebut);
		    dateF = dateFormat.parse(dateFin);
		} catch (ParseException e) {
			throw new ParameterErrorException();
		}
		OracleReservationDAO oracleReservationDAO = new OracleReservationDAO();
		oracleReservationDAO.supprimerReservationLogement(numL,numR, dateD, dateF);	
	}

	public void validerReservationService(String numReservation) throws SQLException, CreationResException, ParameterErrorException {
		
		OracleReservationDAO oracleReservationDAO = new OracleReservationDAO();
		oracleReservationDAO.validerReservation(numReservation);
				
	}

}
