package fr.etu.m1.miage.festibed.model;

public class ReservationFestival {

	
	private String numFestival;
	private String numReservation;
	
	private String nbCatg0;
	private String nbCatg1;
	private String nbCatg2;
	
	private String dateRes;

	public ReservationFestival(String numFestival, String numReservation, String nbCatg0, String nbCatg1,
			String nbCatg2, String dateRes) {
		super();
		this.numFestival = numFestival;
		this.numReservation = numReservation;
		this.nbCatg0 = nbCatg0;
		this.nbCatg1 = nbCatg1;
		this.nbCatg2 = nbCatg2;
		this.dateRes = dateRes;
	}

	public String getNumFestival() {
		return numFestival;
	}

	public void setNumFestival(String numFestival) {
		this.numFestival = numFestival;
	}

	public String getNumReservation() {
		return numReservation;
	}

	public void setNumReservation(String numReservation) {
		this.numReservation = numReservation;
	}

	public String getNbCatg0() {
		return nbCatg0;
	}

	public void setNbCatg0(String nbCatg0) {
		this.nbCatg0 = nbCatg0;
	}

	public String getNbCatg1() {
		return nbCatg1;
	}

	public void setNbCatg1(String nbCatg1) {
		this.nbCatg1 = nbCatg1;
	}

	public String getNbCatg2() {
		return nbCatg2;
	}

	public void setNbCatg2(String nbCatg2) {
		this.nbCatg2 = nbCatg2;
	}

	public String getDateRes() {
		return dateRes;
	}

	public void setDateRes(String dateRes) {
		this.dateRes = dateRes;
	}
	
}
