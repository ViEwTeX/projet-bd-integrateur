package fr.etu.m1.miage.festibed.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.etu.m1.miage.festibed.exception.CreationResException;
import fr.etu.m1.miage.festibed.exception.ParameterErrorException;
import fr.etu.m1.miage.festibed.service.ReservationService;

public class ValiderReservationServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpFormater.formatHeader(resp);
		resp.setContentType("application/json");
		ReservationService reservationService = new ReservationService();
		try {
			reservationService.validerReservationService(req.getParameter("numReservation"));
		} catch (SQLException | CreationResException | ParameterErrorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		resp.setStatus(HttpServletResponse.SC_OK);
	}

}
