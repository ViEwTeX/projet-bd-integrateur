package fr.etu.m1.miage.festibed.model;

public class Utilisateur {
	private String numUtilisateur;
	
	public Utilisateur(String numUtilisateur) {
		super();
		this.numUtilisateur = numUtilisateur;
	}

	public String getNumUtilisateur() {
		return numUtilisateur;
	}

	public void setNumUtilisateur(String numUtilisateur) {
		this.numUtilisateur = numUtilisateur;
	}
	
}
