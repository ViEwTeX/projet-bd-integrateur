package fr.etu.m1.miage.festibed.model;

public class ReservationBis {
	
	private int numReservation;
	private String dateReservation;
	private String dateValidation;
	private String etatReservation;
	private float montantTotal;
	private int nbEnfant;
	private int nbAdulte;
	private int numUtilisateur;
	
	public ReservationBis(int numReservation, String dateReservation, String dateValidation, String etatReservation,
			float montantTotal, int nbEnfant, int nbAdulte, int numUtilisateur) {
		super();
		this.numReservation = numReservation;
		this.dateReservation = dateReservation;
		this.dateValidation = dateValidation;
		this.etatReservation = etatReservation;
		this.montantTotal = montantTotal;
		this.nbEnfant = nbEnfant;
		this.nbAdulte = nbAdulte;
		this.numUtilisateur = numUtilisateur;
	}

	public int getNumReservation() {
		return numReservation;
	}

	public void setNumReservation(int numReservation) {
		this.numReservation = numReservation;
	}

	public String getDateReservation() {
		return dateReservation;
	}

	public void setDateReservation(String dateReservation) {
		this.dateReservation = dateReservation;
	}

	public String getDateValidation() {
		return dateValidation;
	}

	public void setDateValidation(String dateValidation) {
		this.dateValidation = dateValidation;
	}

	public String getEtatReservation() {
		return etatReservation;
	}

	public void setEtatReservation(String etatReservation) {
		this.etatReservation = etatReservation;
	}

	public float getMontantTotal() {
		return montantTotal;
	}

	public void setMontantTotal(float montantTotal) {
		this.montantTotal = montantTotal;
	}

	public int getNbEnfant() {
		return nbEnfant;
	}

	public void setNbEnfant(int nbEnfant) {
		this.nbEnfant = nbEnfant;
	}

	public int getNbAdulte() {
		return nbAdulte;
	}

	public void setNbAdulte(int nbAdulte) {
		this.nbAdulte = nbAdulte;
	}

	public int getNumUtilisateur() {
		return numUtilisateur;
	}

	public void setNumUtilisateur(int numUtilisateur) {
		this.numUtilisateur = numUtilisateur;
	}
	
}
