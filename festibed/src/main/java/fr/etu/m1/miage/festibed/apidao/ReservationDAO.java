package fr.etu.m1.miage.festibed.apidao;

import java.sql.SQLException;
import java.util.Date;
import java.util.Iterator;

import fr.etu.m1.miage.festibed.exception.CreationResException;
import fr.etu.m1.miage.festibed.model.Reservation;

public interface ReservationDAO {

	Iterator<Reservation> getReservations(String numPersonne);

	int ajouterReservation(String dateValidation, String montanTotal, String nbAdulte, String nbEnfant,
			String numUtilisateur) throws SQLException;

	int validerReservation(String numResa) throws SQLException;

	void expirerReservation(int numResa) throws SQLException;

	int ajouterReservationLogement(int numLogement, Date dateDeb, Date dateFin, int numUtilisateur, int nbAd, int nbEn)
			throws SQLException, CreationResException;
}