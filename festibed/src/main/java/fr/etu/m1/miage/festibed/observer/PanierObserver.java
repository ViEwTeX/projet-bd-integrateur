package fr.etu.m1.miage.festibed.observer;

import java.sql.SQLException;

import fr.etu.m1.miage.festibed.apidao.ReservationDAO;
import fr.etu.m1.miage.festibed.oracledao.OracleReservationDAO;

public class PanierObserver implements Observer<Integer> {

	private ReservationDAO reservationDAO;
	
	public PanierObserver() {
		this.reservationDAO = new OracleReservationDAO();
	}
	
	@Override
	public void update(Observable<? extends Integer> observer, Integer numResa) {
		try {
			this.reservationDAO.expirerReservation(numResa);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}


}
