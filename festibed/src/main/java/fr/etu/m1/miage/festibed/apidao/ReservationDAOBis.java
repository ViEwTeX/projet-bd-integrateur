package fr.etu.m1.miage.festibed.apidao;

import java.util.Iterator;

import fr.etu.m1.miage.festibed.model.ReservationBis;

public interface ReservationDAOBis {

	public Iterator<ReservationBis> recuperationReservationsEnCours(int numUtilisateur);

	public boolean verifNbJoursEgauxReservationsEnCours(int numUtilisateur);

}