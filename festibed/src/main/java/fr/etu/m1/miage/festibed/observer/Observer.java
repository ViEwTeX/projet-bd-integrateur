package fr.etu.m1.miage.festibed.observer;

public interface Observer<U> {
    public void update(Observable<? extends U> observer, U arg);
}
