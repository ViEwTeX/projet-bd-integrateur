	package fr.etu.m1.miage.festibed.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.etu.m1.miage.festibed.exception.CreationResException;
import fr.etu.m1.miage.festibed.exception.ParameterErrorException;
import fr.etu.m1.miage.festibed.service.ReservationService;

public class AjouterFestivalReservationServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1480975624614175757L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpFormater.formatHeader(resp);
		resp.setContentType("application/json");
		ReservationService resService = new ReservationService();
		try {
			resService.ajouterFestival(req.getParameter("numU"), req.getParameter("numF"),
					req.getParameter("nbC0"), req.getParameter("nbC1"), req.getParameter("nbC2"),
					req.getParameter("dateD"), req.getParameter("dateF"));
		} catch (SQLException sqlException) {
			resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;	
		} catch (ParameterErrorException e) {
			resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		} catch (CreationResException e) {
			resp.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			return;
		}
		resp.setStatus(HttpServletResponse.SC_OK);
	}
}