package fr.etu.m1.miage.festibed.apidao;

import java.sql.SQLException;
import java.util.Iterator;

import fr.etu.m1.miage.festibed.model.Organisateur;

public interface OrganisateurDAO {
	
	Iterator<Organisateur> getOrganisateurs();

	int ajouterOrganisateur(String numPersonne) throws SQLException;

	int getNumOrganisateurById(String numPersonne);
}
