package fr.etu.m1.miage.festibed.oracledao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;

import fr.etu.m1.miage.festibed.apidao.HebergeurDAO;
import fr.etu.m1.miage.festibed.database.OracleBdAccess;
import fr.etu.m1.miage.festibed.model.Hebergeur;

public class OracleHebergeurDAO implements HebergeurDAO{

	@Override
	public Iterator<Hebergeur> getHebergeurs() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getNumHebergeurById(int numPersonne) {

		int resultat = 0;
		ResultSet rs;
		String quest = "select numHebergeur from Hebergeur where numPersonne = " + numPersonne;
		try {
			rs = OracleBdAccess.select(quest);
			resultat = rs.getInt("numHebergeur");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return resultat;

	}

	@Override
	public int ajouterHebergeur(String numPersonne) throws SQLException {
		String insert = "insert into Hebergeur values ( (select max(numHebergeur) from Hebergeur) + 1 ,'" + numPersonne + "')";
		return OracleBdAccess.update(insert);
	}

}
