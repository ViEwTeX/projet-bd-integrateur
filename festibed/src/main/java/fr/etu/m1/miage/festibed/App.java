package fr.etu.m1.miage.festibed;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.util.thread.QueuedThreadPool;

import fr.etu.m1.miage.festibed.database.Concurrence;
import fr.etu.m1.miage.festibed.database.OracleBdAccess;
import fr.etu.m1.miage.festibed.servlet.AjouterFestivalReservationServlet;
import fr.etu.m1.miage.festibed.servlet.AjouterHebergeurServlet;
import fr.etu.m1.miage.festibed.servlet.AjouterLogementlReservationServlet;
import fr.etu.m1.miage.festibed.servlet.AjouterOrganisateurServlet;
import fr.etu.m1.miage.festibed.servlet.AuthentifcationServlet;

import fr.etu.m1.miage.festibed.servlet.RechercherFestivalsServlet;
import fr.etu.m1.miage.festibed.servlet.RechercherHebergementMazen;
import fr.etu.m1.miage.festibed.servlet.RechercherLogementServlet;
import fr.etu.m1.miage.festibed.servlet.RechercherReservation;
import fr.etu.m1.miage.festibed.servlet.RechercherUnFestivalServlet;
import fr.etu.m1.miage.festibed.servlet.RechercherUnHebergementServlet;
import fr.etu.m1.miage.festibed.servlet.RecupererDomainesFestivalServlet;
import fr.etu.m1.miage.festibed.servlet.SupprimerFestivalPanier;
import fr.etu.m1.miage.festibed.servlet.SupprimerLogementPanier;
import fr.etu.m1.miage.festibed.servlet.ValiderReservationServlet;
import fr.etu.m1.miage.festibed.servlet.ValiderPanierServlet;

/**
 * Hello world!
 *
 */
public class App {
	private Server server;

	void start() throws Exception {
		int maxThreads = 100;
		int minThreads = 10;
		int idleTimeout = 120;

		OracleBdAccess.connect();

		QueuedThreadPool threadPool = new QueuedThreadPool(maxThreads, minThreads, idleTimeout);

		server = new Server(threadPool);
		ServerConnector connector = new ServerConnector(server);
		connector.setPort(8080);
		server.setConnectors(new Connector[] { connector });

		ServletHandler servletHandler = new ServletHandler();
		server.setHandler(servletHandler);

		servletHandler.addServletWithMapping(AuthentifcationServlet.class, "/authentification");
		servletHandler.addServletWithMapping(RechercherFestivalsServlet.class, "/festivals");
		servletHandler.addServletWithMapping(RecupererDomainesFestivalServlet.class, "/festivals/domaines");
		servletHandler.addServletWithMapping(AjouterOrganisateurServlet.class, "/organisateur");
		servletHandler.addServletWithMapping(AjouterHebergeurServlet.class, "/hebergeur");
		servletHandler.addServletWithMapping(RechercherReservation.class, "/panier");
		servletHandler.addServletWithMapping(RechercherUnFestivalServlet.class, "/festival");
		servletHandler.addServletWithMapping(RechercherUnHebergementServlet.class, "/hebergement");
		servletHandler.addServletWithMapping(RechercherHebergementMazen.class, "/hebergementm");
		servletHandler.addServletWithMapping(RechercherLogementServlet.class, "/logement");
		servletHandler.addServletWithMapping(AjouterFestivalReservationServlet.class, "/reservation/addFestival");
		servletHandler.addServletWithMapping(AjouterLogementlReservationServlet.class, "/reservation/addLogement");
		servletHandler.addServletWithMapping(SupprimerFestivalPanier.class, "/reservation/removeFestival");
		servletHandler.addServletWithMapping(SupprimerLogementPanier.class, "/reservation/removeLogement");
		servletHandler.addServletWithMapping(ValiderReservationServlet.class, "/reservation/validerServlet");
        servletHandler.addServletWithMapping(ValiderPanierServlet.class, "/reservation/validation");
        server.start();

	}

	void stop() throws Exception {
		System.out.println("Server stop");
		server.stop();
		OracleBdAccess.disconnect();
		;
	}

	public static void main(String[] args) throws Exception {
		Concurrence.enableDebug(false);
		App server = new App();
		server.start();
	}
}
