package fr.etu.m1.miage.festibed.service;

import java.util.Iterator;
import java.util.NoSuchElementException;

import fr.etu.m1.miage.festibed.apidao.DAOFactory;
import fr.etu.m1.miage.festibed.apidao.ReservationDAO;
import fr.etu.m1.miage.festibed.exception.JoursLogementFestivalNonEquivalentException;
import fr.etu.m1.miage.festibed.exception.MultipleReservationEnCoursException;
import fr.etu.m1.miage.festibed.exception.RessourceNotFoundException;
import fr.etu.m1.miage.festibed.model.ReservationBis;
import fr.etu.m1.miage.festibed.oracledao.OracleDAOFactory;
import fr.etu.m1.miage.festibed.oracledao.OracleReservationDAOBis;

public class ReservationServiceBis {

	public void checkReservationByUser(int numUtilisateur) throws RessourceNotFoundException, MultipleReservationEnCoursException, JoursLogementFestivalNonEquivalentException {
		
		DAOFactory daoFactory = new OracleDAOFactory(); 
//		ReservationDAO reservationDAO = daoFactory.getReservationDAO();
		OracleReservationDAOBis reservationDAO = new OracleReservationDAOBis();
		// Récupération de la réservation avec l'état "en cours"
		Iterator<ReservationBis> reservations = reservationDAO.recuperationReservationsEnCours(numUtilisateur);
		ReservationBis reservation = null;
		
		int compteur = 0;
		while(reservations.hasNext()) {
			if(compteur == 0) {
				try {
					reservation = reservations.next();
				} catch(NoSuchElementException e) {
					e.printStackTrace();
					throw new RessourceNotFoundException();
				}
			} else {
				throw new MultipleReservationEnCoursException();
			}
			
			compteur++;
		}
		if(compteur == 0) {
			System.out.println("aucune resa");
			throw new RessourceNotFoundException();
		}
		
		//-- On regarde si le nombre de jours de festival est égal au nombre de jours de logement pris
		if(!reservationDAO.verifNbJoursEgauxReservationsEnCours(reservation.getNumReservation())) {
			throw new JoursLogementFestivalNonEquivalentException();
		}

		
//		-- On regarde si le nombre de places de la réservation des festivals est égal au nombre de place réservé des logements
//		-- Comptage du nombre de place des festivals
//		SELECT SUM(f.NBCATEGORIE0 + f.NBCATEGORIE1 + f.NBCATEGORIE2)
//		FROM FESTIVALRESERVATION f 
//		WHERE NUMRESERVATION = 2 ;
//		-- Comptage du nombre de place des logements 
//							(ON NE PEUT PAS COMPTER Y'A PAS DE NBPLACE DANS RESERVATIONLOGEMENT DONC UN COUPLE NE PEUT PAS PRENDRE LES MEME JOURS POUR LE MEME LOGEMENT DANS LA MM RESA
//							 -- OU ALORS IL FAUT METTRE UN ID)
//		SELECT SUM((r.DATEFIN - r.DATEDEBUT) --
//		FROM RESERVATIONLOGEMENT r --
//		WHERE NUMRESERVATION = 2 ; --
		
	}
	
}
