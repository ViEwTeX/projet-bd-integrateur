package fr.etu.m1.miage.festibed.apidao;

import java.sql.SQLException;
import java.util.Iterator;

import fr.etu.m1.miage.festibed.exception.RessourceNotFoundException;
import fr.etu.m1.miage.festibed.model.Disponibilite;
import fr.etu.m1.miage.festibed.model.Festival;

public interface FestivalDAO {

	Iterator<Festival> getFestivals(String motsCles, String ville, String categorie, String dateDebut, String dateFin, int numPage, int taillePage) throws SQLException;
	
	Iterator<String> getDomaines() throws SQLException;

	Festival getFestivalById(int idFestival) throws SQLException;

	void ajouterFestival(Festival festival) throws SQLException;

	Iterator<Disponibilite> getDisponibilites(int idFestival) throws SQLException, RessourceNotFoundException;

}
