package fr.etu.m1.miage.festibed.oracledao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import fr.etu.m1.miage.festibed.apidao.ReservationLogementDAO;
import fr.etu.m1.miage.festibed.database.OracleBdAccess;
import fr.etu.m1.miage.festibed.model.ReservationLogement;

public class OracleReservationLogementDAO implements ReservationLogementDAO{

	@Override
	public int ajouterReservationLogement(String numLogement, String dateDeb, String dateFin ,String numUtilisateur) throws SQLException {
		String insert = "insert into ReservationLogement values (" + numLogement + ",(select numRerservation from Reservation where etatReservation = 'En cours' and numUtilisateur = " + numUtilisateur + "),TO_DATE('" + dateDeb + "','DD/MM/YYYY'),TO_DATE('" + dateDeb + "','DD/MM/YYYY'))";
		return OracleBdAccess.update(insert);
	}

	@Override
	public Iterator<ReservationLogement> getReservationLogement(String numReservation) {
		
		String query ="select R.numLogement, R.numReservation, R.dateDebut, R.dateFin, L.description, L.numHebergement from ReservationLogement R join  Logement L on(R.numLogement=L.numLogement) where numReservation = " + numReservation;
		
		System.out.println(query);
		ResultSet rsLogementRes = null;
		try {
			rsLogementRes = OracleBdAccess.select(query);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ArrayList<ReservationLogement> logementRes = new ArrayList<ReservationLogement>();
		
		try {
			while(rsLogementRes.next()) {
				ReservationLogement res = new ReservationLogement(
						rsLogementRes.getString("numLogement"),			
						rsLogementRes.getString("numReservation"),
						rsLogementRes.getString("dateDebut"),
						rsLogementRes.getString("dateFin"),
						rsLogementRes.getString("description"),
						rsLogementRes.getString("numHebergement"));
				logementRes.add(res);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return logementRes.iterator();
	}

	@Override
	public ReservationLogement getReservationLogementById(String numFestival, String numReservation,
			String dateReservation) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
}
