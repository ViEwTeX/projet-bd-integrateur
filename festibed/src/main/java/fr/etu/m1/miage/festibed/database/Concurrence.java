package fr.etu.m1.miage.festibed.database;

public class Concurrence {

	private static boolean debug = false;

	private volatile static int count = 0;
	
	private static Object o = new Object();

	public static void waitConcurrence() {
		synchronized (o) {
			if (debug) {
				System.out.println("Attente de concurrence nb attente(avant entrée): " + count);
				count++;
				System.out.println("Attente de concurrence nb attente(après entrée): " + count);
				if (count < 2) {
					try {
						o.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					o.notify();
				}
				

				System.out.println("Sortie de concurrence nb attente (avant sortie): " + count);
				count--;
				System.out.println("Sortie de concurrence nb attente (après sortie): " + count);
			}
		}
		
	}

	public static void enableDebug(boolean enable) {
		debug = enable;
	}

}
