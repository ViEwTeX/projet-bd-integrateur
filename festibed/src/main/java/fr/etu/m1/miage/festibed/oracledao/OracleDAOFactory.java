package fr.etu.m1.miage.festibed.oracledao;

import fr.etu.m1.miage.festibed.apidao.DAOFactory;
import fr.etu.m1.miage.festibed.apidao.FestivalDAO;
import fr.etu.m1.miage.festibed.apidao.LogementDAO;
import fr.etu.m1.miage.festibed.apidao.ReservationDAO;
import fr.etu.m1.miage.festibed.exception.HebergementNotImplementedException;

public class OracleDAOFactory implements DAOFactory {

	@Override
	public FestivalDAO getFestivalDAO() {
		return new OracleFestivalDAO();
	}

	@Override
	public LogementDAO getLogementDAO(String type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ReservationDAO getReservationDAO() {

		return new OracleReservationDAO();
	}

}
